package
{
	import com.junkbyte.console.Cc;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import starling.core.Starling;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	import utils.CP;

	/**
	 * Main
	 *
	 *
	 */
	[SWF(frameRate=60, width=750, height=650)]
	public class Main extends Sprite
	{

		public static const LOG:String = "GameRoot";

		private var starling:Starling;

		// Constructor

		public function Main()
		{
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}

		// Methods

		private function initializeFlashConsole():void
		{
			Cc.startOnStage(this, "`");
			Cc.setRollerCaptureKey("1");

			Cc.width = 750;
			Cc.height = 300;
			Cc.visible = false;

			Cc.fpsMonitor = false;
			Cc.memoryMonitor = false;
		}

		private function initializeCheatPanel():void
		{
			var cheatPanel:CP = CP.instance;
			cheatPanel.setSize(300, 200);
			cheatPanel.view.doubleClickEnabled = true;

			stage.doubleClickEnabled = true;
			stage.addEventListener(MouseEvent.DOUBLE_CLICK, function (mouseEvent:MouseEvent):void
			{
				if (mouseEvent.ctrlKey)
				{
					cheatPanel.view.parent ?
							cheatPanel.view.parent.removeChild(cheatPanel.view) :
							stage.addChild(cheatPanel.view);
				}
			});

//			cheatPanel.addTab("common");
			cheatPanel.addTab("system");

			cheatPanel.addToggle("common", "fps", true, function (object:Object):void
			{
				starling.showStats = object.value;
			});
		}

		private function initializeStarling():void
		{
			Starling.handleLostContext = true;

			starling = new Starling(GameRoot, stage);
			starling.simulateMultitouch = false;
			starling.enableErrorChecking = false;
			starling.showStats = true;
			starling.showStatsAt(HAlign.LEFT, VAlign.BOTTOM);

			starling.start();
		}

		// Event handlers

		private function addedToStageHandler(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

			stage.frameRate = 60;

			initializeStarling();
			initializeFlashConsole();
			initializeCheatPanel();
		}

	}
}