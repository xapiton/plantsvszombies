package
{
	import starling.display.Sprite;
	import starling.text.TextField;

	import utils.CP;

	/**
	 * GameRoot
	 *
	 *
	 */
	public class GameRoot extends Sprite
	{

		public static const LOG:String = "GameRoot";

		// Constructor

		public function GameRoot()
		{
			cheatPanel_addMethodsHandler();

			drawTestMessage();
		}

		// Methods

		private function drawTestMessage():void
		{
			var helloTF:TextField = new TextField(200, 200, "Hello Ash!");
			helloTF.alignPivot();
			helloTF.x = 300;
			helloTF.y = 300;

			addChild(helloTF);
		}

		private function cheatPanel_addMethodsHandler():void
		{
			var cheatPanel:CP = CP.instance;

			cheatPanel.addButton("common", "test-log", function (object:Object):void
			{
				// test log
			});
		}

		// Event handlers

	}
}